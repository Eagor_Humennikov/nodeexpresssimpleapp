// подключение express
const express = require("express");
const bodyParser = require("body-parser");
const mongodbRaw = require('mongodb');
const MongoClient = require("mongodb").MongoClient;
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: false});
const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true});
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
  
var worckMongoClient;
mongoClient.connect
(
	function(err, client)
	{
		if(err)
		{
			return console.log(err);
		}
		worckMongoClient = client;
		console.log('connected to mongo');
	}
);

app.get("/", urlencodedParser, function (request, response) 
{
    var responseContext;
	responseContext = '<html>';
	responseContext += '<head>';
    responseContext += '<title>Главная</title>';
    responseContext += '<meta charset="utf-8" />';
	responseContext += '</head>';
	responseContext += '<body>';
	responseContext += '<h1>Главная страница</h1>';
	
	var db = worckMongoClient.db('postsService');
	var collection = db.collection('posts');
	collection.find().toArray(function(err, result) {
		if (err) throw err;
		
		responseContext += '<p>';
		responseContext += '<a href="/login">Логин</a>';
		responseContext += '</p>';
		responseContext += '<p>';
		responseContext += '<a href="/reg">Регистрация</a>';
		responseContext += '</p>';
		for (var i = 0; i < result.length; i++) 
		{
			responseContext += '<hr/>';
			responseContext += '<h3>' + result[i].title +'</h3><br/>';
			responseContext += '<p>' + result[i].content +'</p>';
			responseContext += '<form action="/deleteTitle" method="post">';
			responseContext += '<input type="hidden" value="' + result[i]._id +'" name="id"/>';
			responseContext += '<p>';
			responseContext += '<input type="submit" value="Удалить"/><br/>';
			responseContext += '</p>';
			responseContext += '</form>';
		}
		responseContext += '<hr/>';
		responseContext += '<form action="/postTitle" method="post">';
		responseContext += '<p>';
		responseContext += '<label>Заголовок:</label><br/>';
		responseContext += '<input type="text" name="title"/><br/>';
		responseContext += '</p>';
		responseContext += '<p>';
		responseContext += '<label>Содержание:</label><br/>';
		responseContext += '<textarea name="content"></textarea><br/>';
		responseContext += '</p>';
		responseContext += '<p>';
		responseContext += '<input type="submit" value="Отправить"/><br/>';
		responseContext += '</p>';
		responseContext += '</form>';
		responseContext += '<hr/>';
		responseContext += '</body>';
		responseContext += '<html>';
		
		response.send(responseContext);
	});
});

app.post("/postTitle", urlencodedParser, function (request, response) 
{
    var db = worckMongoClient.db('postsService');
    var collection = db.collection('posts');
    var post = {title: request.body.title, content: request.body.content};
    collection.insertOne(post, function(err, result){
          
        if(err)
		{ 
            return console.log("inser error " + err);
        }
    });
	
	response.redirect("/");
});

app.post("/deleteTitle", urlencodedParser, function (request, response) 
{
    var db = worckMongoClient.db('postsService');
    var collection = db.collection('posts');
	
	collection.deleteOne({_id: new mongodbRaw.ObjectID(request.body.id)}, function(err, results) {
       if (err){
         throw err;
       }
    });
	
	response.redirect("/");
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) 
	  {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) 
	  {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

app.get("/login", urlencodedParser, function (request, response) 
{
	response.sendFile(__dirname + "/login.html");
})
app.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: false })
);


passport.use('local-signup', new LocalStrategy(
  function(username, password, done) 
  {
    User.findOne({ username: username }, function(err, user) {
      if (err)
	  {
        return done(err);
	  }

            if (user) {
                return done(null, false, { message: 'User alrady exist.' });
            } else {
                var newUser = new User();

                newUser.username    = username;
                newUser.password = newUser.generateHash(password);

                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }
    });
  }
));

app.get("/reg", urlencodedParser, function (request, response) 
{
	response.sendFile(__dirname + "/reg.html");
})
app.post('/reg',
  passport.authenticate('local-signup', { successRedirect: '/',
                                   failureRedirect: '/reg',
                                   failureFlash: false })
);


app.listen(3000);